module gitlab.com/rkuslak/groupcalc

go 1.12

replace groupcalc/websockets => ./websockets

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.1
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
)
