package websockets

/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"log"
	"time"
)

// maxResults is the maximum amount of Calculation results our pool will store
const maxResults = 10

// Message is a encapsolation class for messages passed from the client to the
// server and from server to client
type Message struct {
	Type int    `json:"type"`
	Body string `json:"body"`
}

// BroadcastCalculation is a struct holding the data send as a "calculation"
// when sending a results broadcast to a end user
type BroadcastCalculation struct {
	Result string
	User   string
	Time   time.Time
}

// Pool is a containing struct storing the information needed for a long-living
// Websocket client pool on the server.
type Pool struct {
	Join           chan *Client
	Leave          chan *Client
	Refresh        chan *Client
	Broadcast      chan ClientMessage
	NewCalculation chan string
	Clients        []*Client
	Results        []Calculation
}

// NewPool creates a new websocket pool for communicating to and orchestrating
// connections to a consuming client connection
func NewPool() *Pool {
	log.Println("Opening new pool")
	return &Pool{
		Results:        make([]Calculation, 0),
		Join:           make(chan *Client),
		Leave:          make(chan *Client),
		NewCalculation: make(chan string),
		Refresh:        make(chan *Client),
		Broadcast:      make(chan ClientMessage),
	}
}

// AddCalculation Adds a new calculation to the pool of current calculations
func (pool *Pool) AddCalculation(calc Calculation) {
	pool.Results = append(pool.Results, calc)

	for len(pool.Results) > maxResults {
		pool.Results = pool.Results[1:]
	}

	log.Printf("Calculations updated: %v (%d)", pool.Results, len(pool.Results))
}

// CreateCalculationArray .
func (pool *Pool) CreateCalculationArray() []BroadcastCalculation {
	// results := make([]string, 0)
	results := make([]BroadcastCalculation, 0)

	for _, result := range pool.Results {
		// results = append(results, result.String())
		calculation := BroadcastCalculation{
			Time:   result.Time,
			User:   result.Creator,
			Result: result.String(),
		}
		results = append(results, calculation)
	}

	return results
}

// QueueBroadcast is a message containing data passed internally in the
// websocket pool
func (pool *Pool) QueueBroadcast() {
	message := ClientMessage{
		Type:         ClientMessageRESULTS,
		Calculations: pool.CreateCalculationArray(),
	}
	log.Println("Queueing broadcast")
	pool.Broadcast <- message
	log.Println("Queued broadcast")
}

// Start allows the connection pool to being receiving connections until such
// time as the thread pool dies.
func (pool *Pool) Start() {
	for {
		select {
		case client := <-pool.Join:
			log.Println(client.ID, " - New connection added to pool")
			pool.Clients = append(pool.Clients, client)
		case droppedClient := <-pool.Leave:
			// Remove dropped client from pool connections
			log.Println(droppedClient.ID, " - Recieved dropped client msg")
			for idx, client := range pool.Clients {
				if client.ID == droppedClient.ID {
					pool.Clients[idx] = pool.Clients[len(pool.Clients)-1]
					pool.Clients = pool.Clients[:len(pool.Clients)-1]
					break
				}
			}
			log.Println(droppedClient.ID, " - client removed")
		case client := <-pool.Refresh:
			calculations := pool.CreateCalculationArray()
			msg := ClientMessage{Type: ClientMessageRESULTS, Calculations: calculations}
			client.Write(msg)

			break
		case calcString := <-pool.NewCalculation:
			log.Println("Received calculation: ", calcString)
			continue
		case body := <-pool.Broadcast:
			log.Printf("Clients: %v\n", pool.Clients)
			for _, client := range pool.Clients {
				log.Println(client.ID, "Broadcasting message: ", body)
				if err := client.Write(body); err != nil {
					log.Println("ERROR: ", err)
					return
				}
			}
			log.Println("Broadcasting message complete: ", body)
		}
	}
}
