package websockets

/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"log"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

// Client is a struct to hold values relating to persistant a client connection
// to the API, with struct functions relevant to acting on this client and its
// information
type Client struct {
	ID         uuid.UUID
	Connection *websocket.Conn
	Pool       *Pool
}

// ClientMessage contains data for a JSON response from the client that will be
// sent over the open Websocket connection.
type ClientMessage struct {
	Type         string                 `json:"type"`
	Message      string                 `json:"message,omitempty"`
	Calculations []BroadcastCalculation `json:"calculations,omitempty"`
}

const (
	// ClientMessageOK .
	ClientMessageOK = "OK"
	// ClientMessageERROR .
	ClientMessageERROR = "ERROR"
	// ClientMessageRESULTS .
	ClientMessageRESULTS = "RESULTS"
	// ClientMessageREFRESH .
	ClientMessageREFRESH = "REFRESH"
	// ClientMessagePING .
	ClientMessagePING = "PING"
	// ClientMessagePONG .
	ClientMessagePONG = "PONG"
	// ClientMessageCALCULATION .
	ClientMessageCALCULATION = "CALCULATION"
)

// NewClient Creates a new Client instance for the provided websocket pool for
// the given client UUID
func NewClient(id uuid.UUID, connection *websocket.Conn, pool *Pool) *Client {
	client := &Client{
		ID:         id,
		Connection: connection,
		Pool:       pool,
	}

	go client.Reader()
	pool.Join <- client

	return client
}

// Reader keeps a thread live and reads from open connections
func (client *Client) Reader() {
	// var message ClientMessage
	defer func() {
		client.Pool.Leave <- client
		client.Connection.Close()
	}()
	for {
		log.Println(client.ID, " - waiting on message")
		var msg ClientMessage
		err := client.Connection.ReadJSON(&msg)
		if err != nil {
			log.Println(client.ID, " - ", err)
			return
		}

		switch msg.Type {
		case ClientMessagePING:
			client.SendPong()
		case ClientMessageREFRESH:
			client.Pool.Refresh <- client
		case ClientMessageCALCULATION:
			calcString := string(msg.Message)
			log.Println(client.ID, " - Received: ", calcString)

			calc, err := NewCalculationFromString(calcString, client.ID.String())
			if err != nil {
				log.Println(client.ID, "Malformed request")
				message := ClientMessage{Type: ClientMessageERROR, Message: err.Error()}
				if err := client.Connection.WriteJSON(message); err != nil {
					log.Println(client.ID, " - ", err)
					return
				}
				continue
			}

			log.Println(client.ID, " - adding calculation")
			client.Pool.AddCalculation(calc)

			log.Println(client.ID, " - queueing broadcast")
			client.Pool.QueueBroadcast()

			log.Println(client.ID, " - Payload complete: ", calcString)
		default:
			log.Println(client.ID, " - Unhandled Websocket request: ", msg.Type)
		}

	}
}

// SendPong is intended to respond to a consuming client's "ping" request in a
// affirmative manner, allowing the connection to remain open
func (client *Client) SendPong() error {
	msg := ClientMessage{
		Type: ClientMessagePONG,
	}
	return client.Connection.WriteJSON(msg)
}

func (client *Client) Write(msg ClientMessage) error {
	return client.Connection.WriteJSON(msg)
}
