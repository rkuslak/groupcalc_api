package websockets

/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"time"
)

type operandMapFunction func(x, y int) int
type operandMap map[rune]operandMapFunction

// Contains a map of runes to the math operation function for said rune
var validOperands operandMap = operandMap{
	'-': func(x, y int) int { return x - y },
	'+': func(x, y int) int { return x + y },
	'*': func(x, y int) int { return x * y },
	'x': func(x, y int) int { return x * y },
	'X': func(x, y int) int { return x * y },
	'/': func(x, y int) int { return x / y },
}

func isOperand(operand rune) bool {
	for validOperand := range validOperands {
		if validOperand == operand {
			return true
		}
	}
	return false
}

// Calculation is a holder for calculations run by the website. Stores
type Calculation struct {
	Left     int
	Right    int
	Opperand rune
	Result   int
	Time     time.Time
	Creator  string
}

func (calc Calculation) String() string {
	return fmt.Sprintf("%d%c%d=%d", calc.Left, calc.Opperand, calc.Right, calc.Result)
}

// NewCalculationFromString creates a new Calculation from a passed string,
// returning the valid calculation on success or the failure reason as error.
func NewCalculationFromString(equation string, creator string) (Calculation, error) {
	result := Calculation{
		Opperand: ' ',
		Time:     time.Now(),
		Creator:  creator,
	}
	runes := []rune(equation)
	index := 0

	for ; index < len(runes); index++ {
		if runes[index] == ' ' {
			continue
		}
		if runes[index] <= '9' && runes[index] >= '0' {
			result.Left = result.Left*10 + (int)(runes[index]-'0')
			continue
		}
		// not white space or number; if we have a operand we can quit quick
		if isOperand(result.Opperand) {
			return Calculation{}, fmt.Errorf("Multiple operands provided")
		}
		if isOperand(runes[index]) {
			result.Opperand = runes[index]
			index++
			break
		}
		return result, fmt.Errorf("Invalid character %c", runes[index])
	}

	if index >= len(runes) {
		if result.Opperand == ' ' {
			return result, fmt.Errorf("No operand provided")
		}
		return result, fmt.Errorf("Invalid equation")
	}

	for ; index < len(runes); index++ {
		if runes[index] == ' ' {
			continue
		}
		if isOperand(runes[index]) {
			return Calculation{}, fmt.Errorf("Multiple operands provided")
		}
		if runes[index] > '9' || runes[index] < '0' {
			return result, fmt.Errorf("Invalid character %c", runes[index])
		}
		result.Right = result.Right*10 + (int)(runes[index]-'0')
	}

	if result.Opperand == '/' && result.Right == 0 {
		return result, fmt.Errorf("Divide by zero failure")
	}

	result.Result = validOperands[result.Opperand](result.Left, result.Right)
	return result, nil
}
