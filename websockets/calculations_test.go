package websockets

/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"reflect"
	"testing"
)

func TestCalculation(t *testing.T) {
	type testCase struct {
		Name      string
		Want      Calculation
		Have      string
		ExpectErr bool
	}
	testCases := []testCase{
		testCase{
			Name: "Valid test /",
			Want: Calculation{
				Left:     100,
				Right:    2,
				Opperand: '/',
				Result:   50,
			},
			Have:      "100 / 2",
			ExpectErr: false,
		},
		testCase{
			Name: "Valid test *",
			Want: Calculation{
				Left:     1,
				Right:    2,
				Opperand: '*',
				Result:   2,
			},
			Have:      "1 * 2",
			ExpectErr: false,
		},
		testCase{
			Name: "Valid test -",
			Want: Calculation{
				Left:     1,
				Right:    2,
				Opperand: '-',
				Result:   -1,
			},
			Have:      "1-2  ",
			ExpectErr: false,
		},
		testCase{
			Name: "Valid test +",
			Want: Calculation{
				Left:     1,
				Right:    2,
				Opperand: '+',
				Result:   3,
			},
			Have:      "1 + 2",
			ExpectErr: false,
		},
		testCase{
			Name:      "Invalid rune test",
			Have:      "1a + 2",
			ExpectErr: true,
		},
		testCase{
			Name:      "Invalid rune test 2",
			Have:      "1+ b2b",
			ExpectErr: true,
		},
		testCase{
			Name:      "Invalid rune test",
			Have:      "1 ^ 2",
			ExpectErr: true,
		},
		testCase{
			Name:      "Invalid equation test",
			Have:      "1 / 0",
			ExpectErr: true,
		},
		testCase{
			Name:      "Missing operand",
			Have:      "1 2",
			ExpectErr: true,
		},
	}

	for _, test := range testCases {
		result, err := NewCalculationFromString(test.Have, "")
		if (err != nil) != test.ExpectErr {
			t.Errorf("%s\n\tExpected error\n\tGOT %v\nFAILED", test.Name, result)
		}

		// Result will always have time set to now; set to wanted time as not
		// relevant:
		test.Want.Time = result.Time

		if !test.ExpectErr && !reflect.DeepEqual(result, test.Want) {
			t.Errorf("%s - HAVE %v - WANTED %v\nFAILED", test.Name, result, test.Want)
		}
	}
}
