PROJECT_NAME ?= website
# ENV_FILE ?= ../../docker/.env
ENV_FILE ?= ../../docker/.env

build:
	podman build \
		-f docker/Dockerfile \
		-t ${PROJECT_NAME}_groupcalc \
		--label=disable \
		.
	podman run --rm \
		-v $(shell pwd):/workspace:Z \
		--env-file ${ENV_FILE} \
		-it ${PROJECT_NAME}_groupcalc \
		go build

start: build
	podman run --rm -d \
		-v $(shell pwd):/workspace:Z \
		--pod ${PROJECT_NAME} \
		--name ${PROJECT_NAME}_groupcalc \
		--env-file ${ENV_FILE} \
		-it ${PROJECT_NAME}_groupcalc \
		/workspace/groupcalc

stop:
	podman container exists ${PROJECT_NAME}_groupcalc \
		&& podman container rm -f ${PROJECT_NAME}_groupcalc \
		|| true