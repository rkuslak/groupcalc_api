package main

/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/rkuslak/groupcalc/websockets"
)

// uuid "github.com/satori/go.uuid"

// WebsocketBufferSize Default buffer size to use for websocket connections
const WebsocketBufferSize = 1024

func healthCheck(w http.ResponseWriter, r *http.Request) {
	response := map[string]interface{}{}

	body, err := json.Marshal(response)
	if err != nil {
		w.Write([]byte("SERVER ERROR\n"))
		return
	}
	w.Write(body)
}

func setupRoutes(allowedCorsHosts []string, pool *websockets.Pool) *chi.Mux {
	router := chi.NewRouter()

	addWebsocketConnectionToPool := func(w http.ResponseWriter, r *http.Request) {
		websocketConnector(w, r, pool)
	}

	corsConfig := cors.Options{
		AllowedOrigins: allowedCorsHosts,
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{
			"Accept",
			"Authorization",
			"Content-Type",
			"X-CSRF-Token",
		},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}
	router.Use(middleware.Recoverer)
	router.Use(middleware.Logger)
	router.Use(middleware.RequestID)
	router.Use(middleware.StripSlashes)
	router.Use(cors.Handler(corsConfig))

	router.HandleFunc("/", addWebsocketConnectionToPool)
	router.Get("/healthcheck", healthCheck)

	return router
}

func websocketConnector(w http.ResponseWriter, r *http.Request, pool *websockets.Pool) {
	connectionId, _ := uuid.NewRandom()

	log.Println(connectionId, " - Websocket connection initiating")

	websocketConnection := websocket.Upgrader{
		ReadBufferSize:  WebsocketBufferSize,
		WriteBufferSize: WebsocketBufferSize,
	}

	// CORS should be handled by Chi; we don't need to recheck it here
	websocketConnection.CheckOrigin = func(_ *http.Request) bool { return true }

	connection, err := websocketConnection.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	websockets.NewClient(connectionId, connection, pool)
	log.Println(connectionId, " - Websocket connection created")
}

func main() {
	pool := websockets.NewPool()
	go pool.Start()
	log.Printf("Initializing server")

	listenPort := os.Getenv("GROUPCALC_PORT")
	if listenPort == "" {
		listenPort = "8080"
	}
	listenAddress := fmt.Sprintf("%s:%s", "", listenPort)

	// TODO: This should default to none?
	corsAllowedHosts := []string{"*"}
	corsAllowedHostsEnv := os.Getenv("CORS_ALLOWED_HOSTS")
	if corsAllowedHostsEnv != "" {
		corsAllowedHosts = strings.Split(corsAllowedHostsEnv, ";")
	}
	router := setupRoutes(corsAllowedHosts, pool)

	fmt.Printf("Starting server on %s\n", listenAddress)
	log.Fatal(http.ListenAndServe(listenAddress, router))
}
